@extends("master")
@section("judul")
Buat Account Baru!
Sign Up Form
@endsection
@section("content")
    <form action="/kirim" method="post">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="namea"> <br><br>
        <label>Last Name:</label><br>
        <input type="text" name="nameblk"> <br><br>
        <label>Gender:</label> <br>
        <input type="radio" name="gender">Perempuan <br>
        <input type="radio" name="gender">Laki-Laki <br><br>
         <label>Nationality:</label><br>
         <select name="national">
            <option value="">Indonesia</option>
            <option value="">Korea</option>
            <option value="">Singapura</option>
            <option value="">Jepang</option>
            <option value="">Inggris</option>
         </select>
         <br>
         <br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="spoken">Bahasa Indonesia <br>
        <input type="checkbox" name="spoken">English <br>
        <input type="checkbox" name="spoken">Other <br><br>
        <label >Bio:</label><br>
        <textarea cols="30" rows="10"></textarea><br><br>
  
  <input type="submit" value="Kirim">
    </form>
    @endsection