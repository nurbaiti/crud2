@extends('master')

<div>
@section ('judul')
    <h2>Detail Cast</h2>
    @endsection

@section('content')
<h4> {{$cast->id}}</h4>
<h4>{{$cast->name}}</h4>
<h4>{{$cast->umur}}</h4>
<h4>{{$cast->bio}}</h4>

<a href="/cast" class="btn btn-secondary btn-sm">Back</a>
@endsection