@extends('master')

<div>
@section ('judul')
    <h2>List cast</h2>
    @endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>          
                @forelse ($cast as $key => $value)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <th>{{$value->name}}</th>
                        <th>{{$value->umur}}</th>
                        <th>{{$value->bio}}</th>
                        <td>
                            
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>Tidak Ada  Data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        @endsection