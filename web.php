<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/biodata', [HomeController::class, 'bio']);
Route::post('/kirim', [AuthController::class, 'kirim']);

Route::get('/data', function(){
    return view('data-tables');
    });
Route::get('/data-table', function(){
return view('table');
});

//CRUD kategori
//create

Route::get('/cast',[CastController::class,'index']);
Route::get('/cast/create',[CastController::class,'create']);
Route::post('/cast',[CastController::class,'store']);
Route::get('/cast/{kategori_id}',[CastController::class,'show']);
Route::get('/cast/{kategori_id}/edit',[CastController::class,'edit']);
Route::put('/cast/{kategori_id}',[CastController::class,'update']);
Route::delete('/cast/{kategori_id}',[CastController::class,'destroy']);






// Route::get('/', [DosenController::class, 'formulir']);
// Route::post('/si', [DosenController::class, 'kirim']);
 
// Route::get('/master',function(){
//     return view('layout.master');
// }
// );

// Route::get('/', [BlogController::class, 'beranda']);
// Route::get('/blog/tentang', [BlogController::class, 'tentang']);