<?php

// app/Http/Controllers/CastController.php

namespace App\Http\Controllers;

use App\Models\cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function index()
    {
        $cast = DB::table('cast') -> get();
        return view('cast.index', compact('cast'));

    } 

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // Validasi request jika diperlukan
        $request->validate([
            'name' => 'required|string',
            'umur' => 'required|integer',
            'bio' => 'required|string'
        ]);

        // Simpan data ke databases
        DB::table('cast')->insert([
            'name' => $request['name'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');
    }

    public function show($id)
    {
        $cast =  DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'umur' => 'required|integer',
            'bio' => 'required|string'
        ]);

       DB::table('cast')
            ->where('id', $id)
            ->update([
                'name' => $request["name"],
                'umur' => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}

